package ru.zolov.tm.command.project;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.security.RoleType;

public final class ProjectDisplayCommand extends AbstractCommand {
    private final String name = "project-display";
    private final String description = "Display project list";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        for (Project project : serviceLocator.getProjectService().readAll(serviceLocator.getUserService().getCurrentUser().getId())) {
            System.out.println(project);
        }
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}

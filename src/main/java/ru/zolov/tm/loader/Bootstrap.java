package ru.zolov.tm.loader;

import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;
import ru.zolov.tm.repository.UserRepository;
import ru.zolov.tm.service.ProjectService;
import ru.zolov.tm.service.TaskService;
import ru.zolov.tm.service.TerminalService;
import ru.zolov.tm.service.UserService;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements ServiceLocator {
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final ProjectService projectService = new ProjectService(projectRepository, taskRepository);
    private final TaskService taskService = new TaskService(taskRepository);
    private final TerminalService terminalService = new TerminalService();
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = new UserService(userRepository);
    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public List<AbstractCommand> getCommandList() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @Override
    public TerminalService getTerminalService() {
        return terminalService;
    }

    public void registry(AbstractCommand command) throws Exception {
        String cliCommand = command.getName();
        String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) throw new Exception("Wrong command!");
        if (cliDescription == null || cliDescription.isEmpty()) throw new Exception("Wrong description!");
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public void registry(Class... classes) throws Exception {
        for (Class clazz : classes) registry(clazz);
    }

    public void registry(Class clazz) throws IllegalAccessException, InstantiationException, Exception {
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        Object command = clazz.newInstance();
        AbstractCommand abstractCommand = (AbstractCommand) command;
        registry(abstractCommand);
    }

    public void init(Class... classes) throws Exception {
        if (classes == null || classes.length == 0) throw new Exception("Empty list");
        registry(classes);
        initAdmin();
        start();
    }

    public void start() throws Exception {
        System.out.println("+-------------------------------+");
        System.out.println("|    Welcome to Task manager    |");
        System.out.println("+-------------------------------+");

        String command = "";

        while (true) {
            try {
                System.out.print("Enter command: \n");
                command = terminalService.nextLine();
                execute(command);
            } catch (Exception e) {
                System.out.println("No such command!");
            }
        }
    }

    private void execute(String command) throws Exception {
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) throw new Exception("No such command");
        final boolean rolesAllowed = getUserService().isRolesAllowed(abstractCommand.roles());
        final boolean secureCheck = abstractCommand.secure() || (!abstractCommand.secure() && userService.isAuth() && rolesAllowed);
        if (secureCheck) abstractCommand.execute();
        else System.out.println("Error! Access forbidden for this command");
    }

    private void initAdmin() {
        try {
            userService.adminRegistration("admin", "admin");
            userService.userRegistration("user", "user");
        } catch (Exception e) {
            System.out.println("Something went wrong in initAdmin!");
            e.printStackTrace();
        }
    }
}


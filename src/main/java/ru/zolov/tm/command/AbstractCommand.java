package ru.zolov.tm.command;

import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.security.RoleType;

public abstract class AbstractCommand {
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract boolean secure();

    public abstract void execute() throws Exception;

    public RoleType[] roles() {
        return null;
    }
}

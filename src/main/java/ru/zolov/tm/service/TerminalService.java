package ru.zolov.tm.service;

import java.util.Scanner;

public class TerminalService {
    private Scanner scanner = new Scanner(System.in);

    public String nextLine() {
        return scanner.nextLine();
    }

    public Integer nextInt() {
        return scanner.nextInt();
    }
}
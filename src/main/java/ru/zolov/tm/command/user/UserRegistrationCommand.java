package ru.zolov.tm.command.user;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.security.RoleType;

public class UserRegistrationCommand extends AbstractCommand {
    private final String name = "user-registration";
    private final String description = "New user registration";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        try {
            System.out.println("Enter login : ");
            String login = serviceLocator.getTerminalService().nextLine();
            System.out.println("Enter password : ");
            String password = serviceLocator.getTerminalService().nextLine();

            serviceLocator.getUserService().userRegistration(login, password);
            System.out.println("User " + login + " successfully added!");
        } catch (Exception e) {
            System.out.println("Something went wrong in registration command!");
            e.printStackTrace();
        }
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}

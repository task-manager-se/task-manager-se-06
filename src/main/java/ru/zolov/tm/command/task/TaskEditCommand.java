package ru.zolov.tm.command.task;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Task;
import ru.zolov.tm.security.RoleType;

public final class TaskEditCommand extends AbstractCommand {
    private final String name = "task-edit";
    private final String description = "Edit task";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        for (Task task : serviceLocator.getTaskService().readAll()) {
            System.out.println(task);
        }
        System.out.print("Enter task id: ");
        String id = serviceLocator.getTerminalService().nextLine();
        System.out.print("Enter new description: ");
        String description = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getTaskService().update(userId, id, description);
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}

package ru.zolov.tm.command.user;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.security.RoleType;

public class UserAuthorizationCommand extends AbstractCommand {
    private final String name = "user-auth";
    private final String description = "Auth user";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Please enter login: ");
        String login = serviceLocator.getTerminalService().nextLine();

        System.out.println("Enter password: ");
        String password = serviceLocator.getTerminalService().nextLine();
        try {
            serviceLocator.getUserService().setCurrentUser(
                    serviceLocator.getUserService().login(login, password));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Somthing went wrong!");
        }
    }
    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}

package ru.zolov.tm.service;

import ru.zolov.tm.entity.Project;
import ru.zolov.tm.repository.ProjectRepository;
import ru.zolov.tm.repository.TaskRepository;

import java.util.Date;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;
    private TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public ProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void create(String userId, String name, String description, Date start, Date finish) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception("Name input empty");
        if (name == null || name.isEmpty()) throw new Exception("Name input empty");
        Project project = new Project(name);
        project.setUserId(userId);
        project.setDescription(description);
        project.setDateOfStart(start);
        project.setDateOfFinish(finish);
        if (description == null || description.isEmpty()) project.setDescription("empty");
        projectRepository.persist(project);
    }

    public Project read(String userId, String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception("Empty input");
        if (id == null || id.isEmpty()) throw new Exception("Empty input");
        return projectRepository.findOne(userId, id);
    }

    public List<Project> readAll(String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception("Empty input");
        if (projectRepository.findAll(userId) == null || projectRepository.findAll(userId).isEmpty())
            throw new Exception("Empty storage");
        return projectRepository.findAll(userId);
    }

    public void update(String userId, String id, String name, String description, Date start, Date finish) {
        if (userId == null || userId.isEmpty()) return;
        if (id == null || id.isEmpty()) return;
        if (name == null || name.isEmpty()) return;

        projectRepository.update(userId, id, name, description, start, finish);
    }

    public boolean remove(String userId, String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception("Empty input");
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        taskRepository.removeAllByProjectID(userId, id);
        return projectRepository.remove(userId, id);
    }
}

package ru.zolov.tm.repository;

import ru.zolov.tm.api.IRepository;
import ru.zolov.tm.entity.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {
    private Map<String, Task> tasks = new LinkedHashMap<>();

    public void persist(String userId, String id, String name) {
        Task task = new Task(userId, id);
        task.setName(name);
        tasks.put(task.getId(), task);
    }

    public void persist(Task task) {
        tasks.put(task.getId(), task);
    }

    public Task findOne(String id) {
        return tasks.get(id);
    }

    public List<Task> findAll() {
        List<Task> taskList;
        return taskList = new ArrayList<>(tasks.values());

    }

    public List<Task> findTaskByProjId(String id) {
        List<Task> result = null;

        for (Task task : tasks.values()) {
            if (task.getProjectId().equals(id)) {
                result.add(task);
            }
        }
        return result = new ArrayList<>();
    }

    public List<Task> findAllByUserId(String userId) {
        List<Task> taskList = new ArrayList<>();
        for (Task task : tasks.values()) {
            if (task.getUserId().equals(userId)) taskList.add(task);
        }
        return taskList;
    }

    public void update(String userId, String id, String description) {
        Task task = tasks.get(id);
        task.setUserId(userId);
        task.setName(description);
    }

    public boolean remove(String userId, String id) {
        if (tasks.get(id).getUserId().equals(userId)){
        tasks.remove(id);
        return true;
        }
        return false;
    }

    public void removeAll() {
        tasks.clear();
    }

    public boolean removeAllByProjectID(String userId, String id) {
        for (Task task : tasks.values()) {
            if (task.getUserId().equals(userId) && task.getProjectId().equals(id)) {
                tasks.remove(task.getId());
                return true;
            }
        }
        return false;
    }

    public void merge(Task task) {
        if (task == null) return;
        if (tasks.containsKey(task.getId())) {
            update(task.getUserId(), task.getId(), task.getDescription());
        } else {
            persist(task);
        }
    }
}



package ru.zolov.tm.entity;

import java.util.Date;
import java.util.UUID;

public class Project {
    private final String id;
    private String userId;
    private String name;
    private String description;
    private Date dateOfStart;
    private Date dateOfFinish;

    public Project(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }

    public Project(String userId, String name, String description, Date dateOfStart, Date dateOfFinish) {
        this.userId = userId;
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.description = description;
        this.dateOfStart = dateOfStart;
        this.dateOfFinish = dateOfFinish;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateOfStart() {
        return dateOfStart;
    }

    public void setDateOfStart(Date dateOfStart) {
        this.dateOfStart = dateOfStart;
    }

    public Date getDateOfFinish() {
        return dateOfFinish;
    }

    public void setDateOfFinish(Date dateOfFinish) {
        this.dateOfFinish = dateOfFinish;
    }

    @Override
    public String toString() {
        return "Project{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dateOfStart=" + dateOfStart +
                ", dateOfFinish=" + dateOfFinish +
                '}';
    }
}

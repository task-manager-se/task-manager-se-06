package ru.zolov.tm.repository;

import ru.zolov.tm.entity.Project;

import java.util.*;

public class ProjectRepository {
    private Map<String, Project> projects = new LinkedHashMap<>();

    public void persist(Project project) {
        projects.put(project.getId(), project);
    }

    public Project findOne(String userId, String id) throws Exception {
        Project project = projects.get(id);
        if (!project.getUserId().equals(userId)) throw new Exception("Wrong user! Permission denied!");
        return project;
    }

    public List<Project> findAll(String userId) {
        List<Project> projectList = new ArrayList<>(projects.values());
        for (Project project : projects.values()) {
            if (project.equals(userId)) {
                projectList.add(project);
            }
        }
        return projectList;
    }

    public List<Project> findAllByUserId(String userId) {
        List<Project> projectList = new ArrayList<>();
        for (Project project : projects.values()) {
            if (project.getUserId().equals(userId)) projectList.add(project);
        }
        return projectList;
    }

    public void update(String userId, String id, String name, String description, Date start, Date finish) {
        Project project = projects.get(id);
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateOfStart(start);
        project.setDateOfFinish(finish);
    }

    public boolean remove(String userId, String id) {
        if (projects.get(id).getUserId().equals(userId)) {
            projects.remove(id);
            return true;
        }
        return false;
    }

    public void removeAll() {
        projects.clear();
    }

    public void merge(Project project) {
        if (project == null) return;
        if (projects.containsKey(project.getId())) {
            update(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getDateOfStart(), project.getDateOfFinish());
        } else {
            persist(project);
        }
    }
}

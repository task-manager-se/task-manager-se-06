package ru.zolov.tm.api;

import java.util.List;

public interface IRepository<E> {

    void persist(E entity);

    E findOne(String id);

    List<E> findAll();

    void merge(E entity);

    boolean remove(String id);

    void removeAll();
}

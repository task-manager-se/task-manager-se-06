package ru.zolov.tm.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Hash {
    public static String getHash(String pass) {
        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance("MD5");
            digest.reset();
            digest.update(pass.getBytes("utf-8"));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String md5Hex = new BigInteger(1, digest.digest()).toString(16);

        while (md5Hex.length() < 32) {
            md5Hex = "0" + md5Hex;
        }

        return md5Hex;
    }
}

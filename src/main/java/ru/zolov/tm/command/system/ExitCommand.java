package ru.zolov.tm.command.system;

import ru.zolov.tm.command.AbstractCommand;


public class ExitCommand extends AbstractCommand {
    private final String name = "exit";
    private final String description = "Command for shut down program";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        Runtime.getRuntime().exit(0);
    }
}

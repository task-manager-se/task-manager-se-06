package ru.zolov.tm.command.project;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.security.RoleType;

import java.util.Date;

public final class ProjectEditCommand extends AbstractCommand {
    private final String name = "project-edit";
    private final String description = "Edit project";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        User user = serviceLocator.getUserService().getCurrentUser();
        System.out.print("Enter project id: ");
        String id = serviceLocator.getTerminalService().nextLine();

        System.out.print("Enter new description: ");
        String description = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getProjectService().update(user.getId(), id, name, description, new Date(), new Date());
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}

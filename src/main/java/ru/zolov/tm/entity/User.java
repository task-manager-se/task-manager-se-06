package ru.zolov.tm.entity;

import ru.zolov.tm.security.RoleType;

import java.util.UUID;

public class User {
    private String login = "";
    private String passwordHash = "";
    private String id = UUID.randomUUID().toString();
    private RoleType role = RoleType.USER;

    public User(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }
}

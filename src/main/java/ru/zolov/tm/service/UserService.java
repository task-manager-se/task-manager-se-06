package ru.zolov.tm.service;

import ru.zolov.tm.entity.User;
import ru.zolov.tm.repository.UserRepository;
import ru.zolov.tm.security.RoleType;
import ru.zolov.tm.util.MD5Hash;

import java.util.Arrays;
import java.util.List;

public class UserService {
    private UserRepository userRepository;
    private User currentUser = null;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public boolean isAuth() {
        return currentUser != null;
    }

    public User login(String login, String password) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception("Empty login");
        if (password == null || password.isEmpty()) throw new Exception("Empty password");
        User user = userRepository.findByLogin(login);
        if (user == null) throw new Exception("UserNotFound");
        String passwordHash = MD5Hash.getHash(password);
        if (passwordHash.equals(user.getPasswordHash())) return user;
        else return null;
    }

    public void userRegistration(String login, String password) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception("Empty login");
        if (password == null || password.isEmpty()) throw new Exception("Empty password");
        if (userRepository.findByLogin(login) != null) throw new Exception("User not found");
        User newUser = new User(login);
        newUser.setRole(RoleType.USER);
        newUser.setPasswordHash(MD5Hash.getHash(password));
        userRepository.persist(newUser);
    }

    public void adminRegistration(String login, String password) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception("Empty login");
        if (password == null || password.isEmpty()) throw new Exception("Empty password");
        if (userRepository.findByLogin(login) != null) throw new Exception("User already exist");
        User admin = new User(login);
        admin.setRole(RoleType.ADMIN);
        admin.setPasswordHash(MD5Hash.getHash(password));
        userRepository.persist(admin);
    }

    public void logOut() {
        currentUser = null;
    }

    public boolean isRolesAllowed(RoleType... roleTypes) {
        if (roleTypes == null) return false;
        if (currentUser == null || currentUser.getRole() == null) return false;
        final List<RoleType> types = Arrays.asList(roleTypes);
        return types.contains(currentUser.getRole());
    }

    public User findByLogin(String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception("Incorrect login");
        return userRepository.findByLogin(login);
    }

    public boolean remove(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        userRepository.remove(id);
        return userRepository.remove(id);
    }

    public void updateUserPassword(String id, String password) throws Exception {
        User user = userRepository.findOne(id);
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        if (password == null || password.isEmpty()) throw new Exception("Password input empty");
        user.setPasswordHash(MD5Hash.getHash(password));
        userRepository.merge(user);
    }

}

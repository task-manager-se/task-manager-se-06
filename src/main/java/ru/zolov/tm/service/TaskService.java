package ru.zolov.tm.service;

import ru.zolov.tm.entity.Task;
import ru.zolov.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public TaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void create(String userId, String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception("Name input empty");
        taskRepository.persist(new Task(userId, name));
    }

    public List<Task> readAll() throws Exception {
        if (taskRepository.findAll() == null || taskRepository.findAll().isEmpty())
            throw new Exception("Empty storage");
        return taskRepository.findAll();
    }

    public List<Task> readTaskByProjectId(String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        return taskRepository.findTaskByProjId(id);
    }

    public Task readTaskById(String id) {
        return taskRepository.findOne(id);
    }

    public boolean remove(String userId, String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        return taskRepository.remove(userId, id);
    }

    public void update(String userId, String id, String description) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception("Id input empty");
        taskRepository.update(userId, id, description);
    }

}

package ru.zolov.tm.command.user;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.security.RoleType;

public class UserPasswordUpdateCommand extends AbstractCommand {
    private final String name = "user-password-upd";
    private final String description = "Update password";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        User user = serviceLocator.getUserService().getCurrentUser();
        System.out.println("Enter new password: ");
        String password = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getUserService().updateUserPassword(user.getId(), password);
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.USER, RoleType.ADMIN};
    }
}

package ru.zolov.tm.repository;

import ru.zolov.tm.api.IRepository;
import ru.zolov.tm.entity.User;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class UserRepository implements IRepository<User> {
    private Map<String, User> users = new LinkedHashMap<>();

    @Override
    public void persist(User user) {
        users.put(user.getId(), user);
    }

    @Override
    public List<User> findAll() {
        List<User> usersList = new ArrayList<>(users.values());
        return usersList;
    }

    @Override
    public User findOne(String id) {
        return users.get(id);
    }

    public User findByLogin(String login) {
        User foundedUser = null;
        for (User user : users.values()) {
            if (user.getLogin().equals(login)) foundedUser = user;
        }
        return foundedUser;
    }

    @Override
    public void merge(User user) {
        if (users.containsKey(user.getId())) {
            update(user.getId(), user.getLogin());
        } else {
            persist(user);
        }
    }

    @Override
    public boolean remove(String id) {
        return users.remove(id) != null;
    }

    @Override
    public void removeAll() {
        users.clear();
    }

    public void update(String id, String login) {
        User user = users.get(id);
        user.setLogin(login);
    }
}

package ru.zolov.tm.command.project;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.security.RoleType;

import java.util.Date;


public final class ProjectCreateCommand extends AbstractCommand {
    private final String name = "project-create";
    private final String description = "Create new project";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.print("Enter project name:");
        String projectName = serviceLocator.getTerminalService().nextLine();

        System.out.print("Enter description: ");
        String projectDescription = serviceLocator.getTerminalService().nextLine();

        serviceLocator.getProjectService().create(userId, projectName, projectDescription, new Date(), new Date());
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}

package ru.zolov.tm.command.task;

import ru.zolov.tm.api.ServiceLocator;
import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.User;
import ru.zolov.tm.security.RoleType;

public final class TaskCreateCommand extends AbstractCommand {
    private final String name = "task-create";
    private final String description = "Create new task";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        User user = serviceLocator.getUserService().getCurrentUser();
        serviceLocator.getTaskService().readAll();
        System.out.print("Enter project id: ");
        String projectId = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getTaskService().create(user.getId(), projectId);
        serviceLocator.getTaskService().readTaskByProjectId(projectId);
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}

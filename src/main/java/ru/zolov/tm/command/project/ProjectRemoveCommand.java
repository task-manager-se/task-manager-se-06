package ru.zolov.tm.command.project;

import ru.zolov.tm.command.AbstractCommand;
import ru.zolov.tm.entity.Project;
import ru.zolov.tm.security.RoleType;

public final class ProjectRemoveCommand extends AbstractCommand {
    private final String name = "project-remove";
    private final String description = "Remove project";

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        for (Project project : serviceLocator.getProjectService().readAll(serviceLocator.getUserService().getCurrentUser().getId())) {
            System.out.println(project);
        }
        System.out.print("Enter project id: ");
        String projectId = serviceLocator.getTerminalService().nextLine();
        System.out.println(serviceLocator.getProjectService().remove(serviceLocator.getUserService().getCurrentUser().getId(), projectId));
    }

    @Override
    public RoleType[] roles() {
        return new RoleType[]{RoleType.ADMIN, RoleType.USER};
    }
}
